import { authenticationService } from '../services/AuthenticationService';

export function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        const message = (data && data.message) || response.statusText;
        const status = response.status;
        const res = {
            "status": status,
            "message": message,
            "data": data
        };

        if (!response.ok) {
            if ([401, 403].indexOf(response.status) !== -1) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                authenticationService.logout();
            }

            return Promise.reject(res);
        }

        return res;
    });
}
