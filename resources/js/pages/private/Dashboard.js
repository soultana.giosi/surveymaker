import React, {Component} from "react";
import {surveyService} from "../../services/SurveyService";

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            surveys: []
        };
    }

    newSurvey() {
        location.assign('/create-survey');
    }

    componentDidMount() {
        surveyService.getAll()
            .then(response => {
                this.setState({ surveys: response });
            });
    }

    renderSurveyItem(survey, index) {
        return <li className="list-group-item" key={'survey' + index}>
                    <a href={"http://"+ window.$serverIp +"/surveys-answers/" + survey.id} target="_blank">
                        {survey.name}
                    </a>

                    <div className="float-right">
                        <a href={"http://"+ window.$serverIp +"/surveys/" + survey.id} target="_blank">
                            <span className="fas fa-external-link-alt"></span>
                        </a>
                    </div>

                    <div className="float-right" style={{ paddingRight: 10 }}>
                        <a href={"http://"+ window.$serverIp +"/api/surveys/" + survey.id + "/export"} target="_blank">
                            <span className="fas fa-download"></span>
                        </a>
                    </div>

                    <div className="float-right" style={{ paddingRight: 10 }}>
                        Respondends: { survey.respondents_count }
                    </div>
                </li>;
    }

    renderSurveyList(surveys) {
        console.log(surveys);
        let surveyItems = [];
        for (const [index, survey] of surveys.entries()) {
            surveyItems.push(this.renderSurveyItem(survey, index))
        }
        return <ul className="list-group">
                    {surveyItems}
                </ul>;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="col-sm-10 offset-sm-1">
                    <br/>
                    <span className="h5">Surveys</span> <button className="btn btn-link" onClick={this.newSurvey}>New</button>
                    <br/>
                    {this.renderSurveyList(this.state.surveys)}
                </div>
            </div>
        )
    }
}

export default Dashboard;
