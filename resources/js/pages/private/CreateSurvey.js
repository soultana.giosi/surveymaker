import React, {Component} from "react";
import SurveyCreator from "../../components/forms/creator/SurveyCreator";

class CreateSurvey extends Component {

    render() {
        return (
            <div className="container-fluid">
                <SurveyCreator />
            </div>
        )
    }
}

export default CreateSurvey;
