import React, {Component} from "react";
import {Link} from "react-router-dom";
import { authenticationService } from '../../services/AuthenticationService';

class Header extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {isLoggedIn: authenticationService.isAuthenticated()}
    }

    anonymousLeftMenu() {
        return (
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link to={'/'} className="nav-link"> Home </Link>
                </li>
                <li className="nav-item">
                    <Link to={'/login'} className="nav-link">Login</Link>
                </li>
                <li className="nav-item">
                    <Link to={'/sign-up'} className="nav-link">Sign Up</Link>
                </li>
            </ul>
        );

    }

    anonymousRightMenu() {
        return (
            <ul className="navbar-nav ml-auto">
            </ul>
        );

    }

    loggedInLeftMenu() {
        return (
            <ul className="navbar-nav">

                <li className="nav-item">
                    <Link to={'/dashboard'} className="nav-link"> Dashboard </Link>
                </li>
            </ul>
        );
    }

    loggedInRightMenu() {
        return (
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                    <Link onClick={authenticationService.logout} className="nav-link"> Logout </Link>
                </li>
            </ul>
        );
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light">

                    { this.state.isLoggedIn
                        ? <Link to={'/dashboard'} className="navbar-brand">SurveyMaker</Link>
                        : <Link to={'/'} className="navbar-brand">SurveyMaker</Link>
                    }

                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarNav">
                        { this.state.isLoggedIn ? this.loggedInLeftMenu() : this.anonymousLeftMenu()}
                        { this.state.isLoggedIn ? this.loggedInRightMenu() : this.anonymousRightMenu()}
                    </div>

                </nav>
            </div>
        )
    }
}

export default Header;
