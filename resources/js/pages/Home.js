import React, {Component} from "react";
import {Link} from "react-router-dom";
import headerImage from "../../images/Survey.jpeg";

class Home extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row row-no-padding" style={{backgroundColor: '#088FCA', color: '#ffffff'}}>
                        <div className="col-lg-12">
                            <img src={headerImage} className="mx-auto d-block" style={{width:'100%', maxWidth:"550px"}}/>
                        </div>
                        <div className="col-lg-12">
                            <div className="text-center" style={{float: 'center'}}>
                                <span style={{ fontSize: '50px', fontWeight: 'bold'}}>
                                    A simple & powerful online survey tool
                                </span>
                                <span style={{fontSize: '50px', fontWeight: 'bold'}}>
                                </span>
                                <br /><br />
                                <span style={{fontSize: '20px', fontWeight: 'bold'}}>
                                    Sign up now for free unlimited surveys, questions & responses.
                                </span>
                                <br /><br /><br />
                                <Link to="/sign-up" >
                                    <button className="btn btn-outline-info btn-lg" style={{color: '#ffffff', borderColor: '#ffffff', marginBottom: '50px',}}>
                                        Sign Up
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <br />
                    <div className="col-lg-12 text-center">
                        <span style={{ fontSize: '25px', fontWeight: 'bold'}}>
                        Join over 150 users who rely on SurveyMaker for People Powered Data
                        </span>
                    </div>
                    <br />
                    <div className="card-deck mb-3 text-center">
                        <div className="card mb-4 box-shadow">
                            <div className="card-header"  style={{backgroundColor: '#088FCA', color: '#ffffff'}}>
                                <h4 className="my-0 font-weight-normal">Surveys</h4>
                            </div>
                            <div className="card-body" >
                                <span style={{ fontSize: '50px', fontWeight: 'bold'}}>345</span>
                            </div>
                        </div>
                        <div className="card mb-4 box-shadow">
                            <div className="card-header"  style={{backgroundColor: '#088FCA', color: '#ffffff'}}>
                                <h4 className="my-0 font-weight-normal">Questions</h4>
                            </div>
                            <div className="card-body">
                                <span style={{ fontSize: '50px', fontWeight: 'bold'}}>1830</span>
                            </div>
                        </div>
                        <div className="card mb-4 box-shadow">
                            <div className="card-header"  style={{backgroundColor: '#088FCA', color: '#ffffff'}}>
                                <h4 className="my-0 font-weight-normal">Responses</h4>
                            </div>
                            <div className="card-body">
                                <span style={{ fontSize: '50px', fontWeight: 'bold'}}>4680</span>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>

                <div className="container-fluid" style={{backgroundColor: '#088FCA', color: '#ffffff'}}>
                    <div className="row row-no-padding" >
                        <div className="col-md-3"></div>
                        <div className="col-md-6">
                            <div className="text-center" style={{float: 'center', paddingTop: '80px', paddingBottom: '80px'}}>
                                <span style={{ fontSize: '25px', fontWeight: 'bold'}}>
                                    A survey is not just an online tool that helps you jot down a list of questions. It’s a great way to gain insights into what your audience thinks, feels, and most importantly... what it wants.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container" style={{backgroundColor: '#ffffff', color: '#000000', paddingTop: '40px', paddingBottom: '40px'}}>
                    <div className="row row-no-padding" >
                        <div className="col-lg-12 text-center">
                            <span style={{ fontSize: '45px', fontWeight: 'bold'}}>
                            Features
                            </span>
                        </div>
                    </div>
                    <div className="row row-no-padding" style={{paddingTop: '40px'}}>
                        <div className="col-md-6 text-center"><span style={{ fontSize: '150px', color: '#088FCA'}} className="fas fa-clock"></span></div>
                        <div className="col-md-6">
                            <div className="text-center" style={{float: 'center'}}>
                                <span style={{ fontSize: '25px', fontWeight: 'bold'}}>
                                    Fast & Easy
                                </span>

                            </div>
                            <div className="text-center" style={{float: 'center'}}>

                            <span  style={{ fontSize: '18px'}}>
                                    We've done our best to make creating surveys as enjoyable as possible. We've got the fastest and most intuitive user experience on the block. Creating online surveys has never been more fun and hassle-free.
                             </span>
                            </div>
                        </div>
                    </div>
                    <br /><br /><br />
                    <div className="row row-no-padding" >
                        <div className="col-md-6">
                            <div className="text-center" style={{float: 'center'}}>
                                <span style={{ fontSize: '25px', fontWeight: 'bold'}}>
                                    Responsive Layout
                                </span>

                            </div>
                            <div className="text-center" style={{float: 'center'}}>

                            <span  style={{ fontSize: '18px'}}>
                                Our surveys work on mobile devices, tablets and desktop computers. Before you share your survey, just head into Preview mode to see how it looks on different devices.
                            </span>
                            </div>
                        </div>
                        <div className="col-md-6 text-center"><span style={{ fontSize: '150px', color: '#088FCA'}} className="fas fa-mobile"></span></div>
                    </div>
                </div>


                <div className="container-fluid" style={{backgroundColor: '#088FCA', color: '#ffffff'}}>
                    <div className="row row-no-padding" >
                        <div className="col-md-3"></div>
                        <div className="col-md-6">
                            <div className="text-center" style={{float: 'center', paddingTop: '80px', paddingBottom: '80px'}}>
                                <span style={{ fontSize: '55px', fontWeight: 'bold'}} >Sign Up Now</span><br />
                                <span style={{ fontSize: '25px', fontWeight: 'bold'}}>
                                    Free unlimited surveys, questions & responses.
                                </span>
                                <br />
                                <br />
                                <br />
                                <Link to="/sign-up" >
                                    <button className="btn btn-outline-info btn-lg" style={{color: '#ffffff', borderColor: '#ffffff', marginBottom: '50px',}}>
                                        Sign Up
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )
    }
}

export default Home;
