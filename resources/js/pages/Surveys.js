import React, {Component} from "react";
import Survey from "../components/forms/respondent/Survey";

class Surveys extends Component {

    render() {
        return (
            <div className="container-fluid">
                <Survey surveyId={this.props.match.params.surveyId}/>
            </div>
        )

    }
}

export default Surveys;
