import React, {Component} from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import { authenticationService } from '../services/AuthenticationService';
import {Redirect} from "react-router-dom";

class LogIn extends Component {

    constructor(props) {
        super(props);
        this.state = {email: "", password: "", isLoggedInSuccessful: false};
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleSubmit(event) {
        event.preventDefault();

        authenticationService.login(this.state.email, this.state.password)
            .then(user => location.assign('/dashboard'));
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row row-no-padding" >
                    <div className="col-sm-3"></div>
                    <div className="Login col-sm-6">
                        <form onSubmit={this.handleSubmit}>
                            <FormGroup controlId="email">
                                <FormLabel>Email</FormLabel>
                                <FormControl autoFocus type="email"
                                    onChange={e => this.setState({ email: e.target.value})}
                                />
                            </FormGroup>
                            <FormGroup controlId="password">
                                <FormLabel>Password</FormLabel>
                                <FormControl type="password"
                                    onChange={e => this.setState({ password: e.target.value})}
                                />
                            </FormGroup>
                            <Button block disabled={!this.validateForm()} type="submit">
                                Login
                            </Button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default LogIn;
