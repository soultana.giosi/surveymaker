import React, {Component} from "react";
import {AnswerTypes } from "../../FrontendAnswerType";
import TextQuestion from "./TextQuestion";
import ScaleQuestion from "./ScaleQuestion";
import MultipleChoiceQuestion from "./MultipleChoiceQuestion";

export const QuestionFactory = {

    createForConfig(id, config, updateCallback)
    {
        switch (config.answer.type) {
            case AnswerTypes.TEXT:
                return <TextQuestion
                    key={'question' + id}
                    id = {id}
                    config={config}
                    updateParentState={updateCallback}
                />;
            case AnswerTypes.SCALE:
                return <ScaleQuestion
                    key={'question' + id}
                    id = {id}
                    config={config}
                    updateParentState={updateCallback}
                />;
            case AnswerTypes.MULTIPLE:
                return <MultipleChoiceQuestion
                    key={'question' + id}
                    id = {id}
                    config={config}
                    updateParentState={updateCallback}
                />;
            default:
                throw new Error("Unsupported type passed");
        }

        //this.props.updateQuestionConfig(this.state.config);
    }
};
