import React, {Component} from "react";

class ScaleQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            config: props.config,
        };
    }

    updateParentState() {
        this.props.updateParentState(this.state.id, this.state.config);
    }

    updateAnswer(configAttribute, value) {
        this.state.config.answer[configAttribute] = value;
        this.updateParentState();
        this.forceUpdate();
    }

    updateQuestionText(value) {
        this.state.config.text = value;
        this.updateParentState();
        this.forceUpdate();
    }

    render() {

        return (
            <div className="container-fluid col-sm-8">
                <div className="card">
                    <div className="card-header">
                        Scale Question
                    </div>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="question">Question text :</label>
                            <input className="form-control" type="text" id='question'
                                   value={this.state.config.text}
                                   onChange={(e) => this.updateQuestionText(e.target.value)} />
                            <label htmlFor="min">Minimum answer value:</label>
                            <input className="form-control" type="number" id='min'
                                   value={this.state.config.answer.min}
                                   onChange={(e) => this.updateAnswer('min', e.target.value)} />
                            <label htmlFor="max">Maximum answer value :</label>
                            <input className="form-control" type="number" id='max'
                                   value={this.state.config.answer.max}
                                   onChange={(e) => this.updateAnswer('max', e.target.value)} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ScaleQuestion;
