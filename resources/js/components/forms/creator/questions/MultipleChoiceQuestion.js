import React, {Component} from "react";
import {RandomId} from "../../../../helpers/RandomId";

class MultipleChoiceQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            config: props.config,
            optionInput: '',
            options: {}
        };
    }

    updateParentState() {
        this.props.updateParentState(this.state.id, this.state.config);
    }

    updateQuestionText(text) {
        this.state.config.text = text;
        this.updateParentState();
        this.forceUpdate();
    }

    handleInputChange(optionInput) {
        this.setState({optionInput: optionInput});
    }

    addOption() {
        this.state.options[RandomId.generate()] = this.state.optionInput;
        this.state.config.answer.options.push({'text':this.state.optionInput});
        this.state.optionInput = '';
        this.updateParentState();
        this.forceUpdate();
    }

    removeOption(id) {
        delete this.state.options[id];
        this.state.config.answer.options = Object.values(this.state.options);
        this.updateParentState();
        this.forceUpdate();
    }

    renderOption(id, option) {
        return (
            <li className={'list-group-item'} key={'option' + id}>
                <button id={'option' + id} className={"btn btn-link"} onClick={(e) => this.removeOption(id)}>X</button>
                {option}
            </li>);
    }

    render() {
        let options = [];
        for (const [index, option] of Object.entries(this.state.options)) {
            options.push(this.renderOption(index, option));
        }

        return (
            <div className="container-fluid col-sm-8">
                <div className="card">
                    <div className="card-header">
                        Multiple Choice Question
                    </div>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="question">Question text :</label>
                            <input className="form-control" type="text" id='question'
                                   value={this.state.config.text}
                                   onChange={(e) => this.updateQuestionText(e.target.value)} />

                            <label htmlFor="optionInput">Answer options :</label>
                            <div className="input-group">
                                <input className="form-control" id='optionInput'
                                       value={this.state.optionInput}
                                       onChange={(e) => this.handleInputChange(e.target.value)} />
                                <button className="btn btn-primary" onClick={(e) => this.addOption()}>+</button>
                            </div>
                            <ul className="list-group list-group-flush">
                            {options}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MultipleChoiceQuestion;
