import {AnswerTypes} from "../../FrontendAnswerType";
import { cloneDeep } from "lodash"

export const QuestionConfig = {
    TEXT: {
        text: '',
        answer: {
            type: AnswerTypes.TEXT,
            min_chars: '0',
            max_chars: '500'
        }
    },
    SCALE: {
        text: '',
        answer: {
            type: AnswerTypes.SCALE,
            min: '0',
            max: '10'
        }
    },
    MULTIPLE: {
        text: '',
        answer: {
            type: AnswerTypes.MULTIPLE,
            options: []
        }
    },
    getForType(type) {
        switch (type) {
            case AnswerTypes.TEXT:
                return cloneDeep(this.TEXT);
            case AnswerTypes.SCALE:
                return cloneDeep(this.SCALE);
            case AnswerTypes.MULTIPLE:
                return cloneDeep(this.MULTIPLE);
            default:
                throw new Error("Unsupported type passed");
        }
    }
};
