import React, {Component} from "react";
import QuestionTypeSelector from "./QuestionTypeSelector";
import { QuestionConfig } from "../creator/questions/QuestionsConfig";
import { QuestionFactory } from "./questions/QuestionFactory";
import { RandomId } from "../../../helpers/RandomId";
import {surveyService} from "../../../services/SurveyService";

class SurveyCreator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            surveySubmitted: false,
            name: null,
            title: null,
            questions: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Submit Survey to backend
     * @param event
     */
    handleSubmit(event) {
        event.preventDefault();
        // create survey
        console.log(Object.values(this.state.questions))

        surveyService.submitCreateSurvey(this.state.name, this.state.title, Object.values(this.state.questions))
            .then(response => {
                console.log(response);
                location.assign('/dashboard');
            }).catch(error => {
                // TODO: do something for the error
                console.log(error)
                console.log('shit hit the fan');
        });
    }

    /**
     * - Adds a question to State based on selected type
     * - rerender
     * @param type
     */
    addQuestionOfType(type) {
        this.state.questions[RandomId.generate()] = QuestionConfig.getForType(type);
        this.forceUpdate();
    }

    /**
     * Render a question
     */
    renderQuestion(id, questionConfig)
    {
        return QuestionFactory.createForConfig(id, questionConfig, this.updateQuestionConfig.bind(this))
    }

    /**
     * Child questions calls this on config change to update parents(this) state
     */
    updateQuestionConfig(id, questionConfig)
    {
        this.state.questions[id] = questionConfig;
    }

    updateName(value) {
        this.state.name = value;
    }

    updateTitle(value) {
        this.state.title = value;
    }

    render() {
        let questions = [];
        for (const [index, question] of Object.entries(this.state.questions)) {
                questions.push(this.renderQuestion(index, question));
        }

        return (
            <div className="container-fluid">

                <div className="col-sm-10 offset-sm-1">
                    <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input className="form-control" id="name"
                               onChange={(e) => this.updateName(e.target.value)}
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input className="form-control" id="title"
                               onChange={(e) => this.updateTitle(e.target.value)}
                        />
                    </div>

                    <h5>Questions</h5>
                    {questions}

                    <br/>
                    <QuestionTypeSelector addTypeSelected={this.addQuestionOfType.bind(this)}/>
                    <br/>
                    <button className="btn btn-primary btn-lg btn-block " onClick={(e) => this.handleSubmit(e)}>Submit</button><br/>
                </div>
            </div>
        )
    }
}

export default SurveyCreator;
