import React, {Component} from "react";
import {AnswerTypes } from "../FrontendAnswerType";

class QuestionTypeSelector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            type: AnswerTypes.MULTIPLE,
        };
    }

    addQuestion() {
        this.props.addTypeSelected(this.state.type);
    }

    handleInputChange(type) {
        this.setState({type: type});
    }

    render() {
        return (
            <div className="container-fluid col-sm-4">
                <label htmlFor="questionType">Select type and add a question</label>

                <div className="input-group">
                    <select value={this.state.type} className="form-control" onChange={(e) => this.handleInputChange(e.target.value)}>
                        <option value={AnswerTypes.TEXT} >Text</option>
                        <option value={AnswerTypes.SCALE} >Scale</option>
                        <option value={AnswerTypes.MULTIPLE} >Multiple Choice</option>
                    </select>
                    <button className="btn btn-primary" onClick={(e) => this.addQuestion()}>+</button><br/>
                </div>
            </div>
        )
    }
}

export default QuestionTypeSelector;
