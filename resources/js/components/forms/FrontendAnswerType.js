export const AnswerTypes = {
    TEXT: 'TextAnswer',
    SCALE: 'ScaleAnswer',
    MULTIPLE: 'MultipleChoiceAnswer'
};
