import React, {Component} from "react";

class TextAnswer extends Component {

    constructor(props) {
        super(props);
    }

    handleInputChange(questionId, answer) {
        this.props.onInputChange(questionId, answer);
    }

    render() {
        return <div className="form-group">
                <textarea className="form-control" rows="3"
                          onChange={(e) => this.handleInputChange(this.props.questionId, e.target.value)}
                />
               </div>
    }
}

export default TextAnswer;
