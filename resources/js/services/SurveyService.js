import { handleResponse } from '../helpers/handle-response';

export const surveyService = {
    getAll,
    getById,
    getAnswersById,
    submitAnswersForSurveyId,
    submitCreateSurvey
};

function getAll() {

    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type':'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': 'Bearer ' + localStorage.getItem('access_token')
        }
    };

    return fetch('http://'+ window.$serverIp +'/api/admin/surveys', requestOptions)
        .then(handleResponse)
        .then(response => {
            return response.data;
        });
}

function getAnswersById(id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type':'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': 'Bearer ' + localStorage.getItem('access_token')
        }
    };

    return fetch('http://'+ window.$serverIp +'/api/admin/surveys/' + id + '/answers', requestOptions)
        .then(handleResponse)
        .then(response => {
            return response.data;
        });
}

function getById(id) {
    // put token here
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type':'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        }
    };

    return fetch('http://'+ window.$serverIp +'/api/surveys/'+id, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response.data;
        });
}

function submitAnswersForSurveyId(answers, id) {

    let formattedAnswers = [];
    for (let [questionId, answer] of Object.entries(answers)) {
        formattedAnswers.push({ "questionId": questionId, "answer": answer})
    }

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: JSON.stringify({"answers": formattedAnswers})
    };

    return fetch('http://'+ window.$serverIp +'/api/surveys/'+id, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
        }).catch(error => {
            return Promise.reject(error);
        });
}

function submitCreateSurvey(name, title, questions) {

    let body = {
        "survey": {
            "name": name,
            "title": title,
            "questions": questions
        }
    };

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': 'Bearer ' + localStorage.getItem('access_token')
        },
        body: JSON.stringify(body)
    };

    return fetch('http://'+ window.$serverIp +'/api/admin/surveys', requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
        }).catch(error => {
            return Promise.reject(error);
        });
}
