import { handleResponse } from '../helpers/handle-response';
import User from '../models/User'

export const authenticationService = {
    login,
    logout,
    getToken,
    isAuthenticated,
    getUser
};

function getToken() {
    return localStorage.getItem('access_token');
}

function isAuthenticated() {
    return (localStorage.getItem('access_token') !== null);
}

function getUser() {
    let userJson = JSON.parse(localStorage.getItem('user'));
    return new User(userJson.name, userJson.email);
}

function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: JSON.stringify({ email: email, password: password })
    };

    return fetch('http://'+ window.$serverIp +'/api/auth/login', requestOptions)
        .then(handleResponse)
        .then(response => {
            let user = new User(response.data.name, response.data.email);
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(user));
            localStorage.setItem('access_token', response.data.access_token);

            return user;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');

    location.assign('/login');
}
