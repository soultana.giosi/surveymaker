<?php


namespace App\Factories;


use App\Question;
use App\RespondentMultipleChoiceAnswer;
use App\RespondentScaleAnswer;
use App\RespondentTextAnswer;
use Exception;

class RespondentAnswerFactory
{
    /**
     * @param string $frontendAnswerType
     * @param int $answerId
     * @param $answer
     * @return RespondentMultipleChoiceAnswer|RespondentScaleAnswer|RespondentTextAnswer
     * @throws Exception
     */
    static public function create(string $frontendAnswerType, int $answerId, $answer)
    {
        switch ($frontendAnswerType) {
            case Question::TEXT_ANSWER:
                return new RespondentTextAnswer([
                    'answer' => $answer
                ]);
            case Question::SCALE_ANSWER:
                return new RespondentScaleAnswer([
                    'answer' => $answer
                ]);
            case Question::MULTIPLE_CHOICE_ANSWER:
                return new RespondentMultipleChoiceAnswer([
                    'multiple_choice_answer_option_id' => $answer
                ]);
            default:
                throw new Exception('Unsupported answer type');
        }
    }
}
