<?php

namespace App\Factories;

use App\MultipleChoiceAnswer;
use App\MultipleChoiceAnswerOption;
use App\Question;
use App\ScaleAnswer;
use App\TextAnswer;
use Exception;
use Illuminate\Database\Eloquent\Model;

class AnswerFactory
{

    /**
     * @param array $input
     * @return Model
     * @throws Exception
     */
    static public function createFromArray(string $frontendAnswerType, array $input)
    {
        switch ($frontendAnswerType) {
            case Question::TEXT_ANSWER:
                return TextAnswer::create([ // TODO: this can create an object instead of actually saving
                    'min_chars' => $input['min_chars'],
                    'max_chars' => $input['max_chars']
                ]);
            case Question::SCALE_ANSWER:
                return ScaleAnswer::create([
                    'min' => $input['min'],
                    'max' => $input['max']
                ]);
            case Question::MULTIPLE_CHOICE_ANSWER:
                $answer = MultipleChoiceAnswer::create();
                // add options to answer
                foreach ($input['options'] as $option) {
                    $answer->options->add(
                        new MultipleChoiceAnswerOption([
                                'text' => $option['text'],
                                'multiple_choice_answer_id' => $answer->id
                            ])
                    );
                }
                $answer->push();
                return $answer;
            default:
                throw new Exception('Unsupported answer type');
        }
    }
}
