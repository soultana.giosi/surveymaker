<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespondentTextAnswer extends Model
{
    protected $fillable = ['answer'];

    public function answer()
    {
        return $this->belongsTo(TextAnswer::class);
    }
}
