<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public const TEXT_ANSWER = 'TextAnswer';
    public const SCALE_ANSWER = 'ScaleAnswer';
    public const MULTIPLE_CHOICE_ANSWER = 'MultipleChoiceAnswer';


    protected $fillable = ['text', 'survey_id', 'answer_id', 'answer_type'];

    protected $visible = ['id', 'text', 'frontend_answer_type', 'answer'];

    protected $appends = ['frontend_answer_type'];

    protected $with = ['answer'];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function answer()
    {
        return $this->morphTo();
    }

    public function getFrontendAnswerTypeAttribute()
    {
        switch (get_class($this->answer)) {
            case TextAnswer::class:
                return self::TEXT_ANSWER;
            case MultipleChoiceAnswer::class:
                return self::MULTIPLE_CHOICE_ANSWER;
            case ScaleAnswer::class:
                return self::SCALE_ANSWER;
            default:
                throw new \Exception('Unsupported Answer type');
        }
    }
}
