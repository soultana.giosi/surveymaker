<?php


namespace App\Services;


use App\Question;
use App\Survey;

class SurveyResultsService
{
    public function getResultsForSurvey(Survey $survey)
    {
        $results = [
            'title' => $survey->title,
            'questions' => []
        ];

        foreach ($survey->questions as $question) {
            /** @var Question $question */

            $results['questions'][] = [
                'frontend_answer_type' => $question->getFrontendAnswerTypeAttribute(),
                'text' => $question->text,
                'results' => (new AnswerResultsService())->getResultsForAnswer($question->answer)
            ];
        }

        return $results;
    }
}
