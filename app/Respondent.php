<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respondent extends Model
{
    protected $fillable = ['email'];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
}
