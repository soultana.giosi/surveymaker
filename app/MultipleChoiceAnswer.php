<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleChoiceAnswer extends Model
{
    protected $with = ['options'];

    protected $visible = ['options'];

    public function question()
    {
        return $this->morphOne(Question::class, 'answer');
    }

    public function options()
    {
        return $this->hasMany(MultipleChoiceAnswerOption::class);
    }

    public function respondentAnswers()
    {
        return $this->hasMany(RespondentMultipleChoiceAnswer::class);
    }
}
