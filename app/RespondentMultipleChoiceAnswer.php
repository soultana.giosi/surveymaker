<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespondentMultipleChoiceAnswer extends Model
{
    protected $fillable = ['multiple_choice_answer_option_id'];

    public function answer()
    {
        return $this->belongsTo(MultipleChoiceAnswer::class);
    }
}
