<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleChoiceAnswerOption extends Model
{
    protected $fillable = ['text', 'multiple_choice_answer_id'];

    protected $visible = ['text', 'id'];

    public function answer()
    {
        return $this->belongsTo(MultipleChoiceAnswer::class);
    }
}
