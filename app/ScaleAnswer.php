<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScaleAnswer extends Model
{
    protected $fillable = ['min', 'max'];
    
    protected $visible = ['min', 'max'];

    public function question()
    {
        return $this->morphOne(Question::class, 'answer');
    }

    public function respondentAnswers()
    {
        return $this->hasMany(RespondentScaleAnswer::class);
    }
}
