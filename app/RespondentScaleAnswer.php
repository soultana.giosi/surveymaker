<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespondentScaleAnswer extends Model
{
    protected $fillable = ['answer'];

    public function answer()
    {
        return $this->belongsTo(ScaleAnswer::class);
    }
}
