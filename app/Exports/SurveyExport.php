<?php

namespace App\Exports;

use App\MultipleChoiceAnswer;
use App\Question;
use App\ScaleAnswer;
use App\Services\AnswerResultsService;
use App\Survey;
use App\TextAnswer;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;

class SurveyExport implements FromCollection, WithMapping
{
    use Exportable;

    public function forSurvey(string $surveyId)
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Survey::where('id', $this->surveyId)->get();
    }

    /**
     * @var Survey $invoice
     */
    public function map($survey): array
    {
        $results = [];
        $i = 0;
        foreach ($survey->questions as $key => $question) {
            /** @var Question $question */

            $results[$i] = [
                $question->text,
                (new AnswerResultsService())->getResultsForAnswer($question->answer),
                get_class($question->answer)
            ];
            $i++;
        }

        $lines = [];
        $i = 0;
        foreach ($results as $result) {
            $lines[$i] = [(string) $result[0]];$i++;

            switch ($result[2]) {
                case TextAnswer::class:
                    $lines[$i] = [
                        'Text Answers'
                    ];$i++;
                    break;
                case MultipleChoiceAnswer::class:
                    $lines[$i] = [
                        'Answer',
                        'Times Answered'
                    ];$i++;
                    break;
                case ScaleAnswer::class:
                    $lines[$i] = [
                        'Scale Answer',
                        'Times Answered'
                    ];$i++;
                    break;
                default:
                    throw new \Exception('Unsupported Answer type');
            }

            foreach ($result[1] as $answer) {

                switch ($result[2]) {
                    case TextAnswer::class:
                        $lines[$i] = [$answer];$i++;
                        break;
                    case MultipleChoiceAnswer::class:
                        $lines[$i] = [
                            (string) $answer->text,
                            $answer->answerCount
                        ];$i++;
                        break;
                    case ScaleAnswer::class:
                        $lines[$i] = [
                            $answer->answer,
                            $answer->answerCount
                        ];$i++;
                        break;
                    default:
                        throw new \Exception('Unsupported Answer type');
                }
            }

            $lines[$i] = []; $i++;
        }

        return $lines;
    }
}
