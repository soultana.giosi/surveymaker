<?php

namespace App\Http\Controllers;

use App\Exports\SurveyExport;
use App\Factories\AnswerFactory;
use App\Question;
use App\Services\SurveyResultsService;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AdminSurveyController extends Controller
{
//  {
//    "survey": {
//    "name" : "survey 2019",
//    "title" : "Environmental changes q",
//    "questions": [
//        {
//            "text": "What do you think",
//            "answer": {
//                    "type": "TextAnswer",
//                    "min_chars": 0,
//                    "max_chars": 300
//                }
//         }
//      ]
//    }
//  }
    public function create(Request $request)
    {
        $input = $request->json()->all();
        $inputSurvey = $input['survey'] ?? null;

        if ($inputSurvey === null) {
            return new Response(null, 400);
        }

        $survey = Survey::create([
            'name' => $inputSurvey['name'],
            'title' => $inputSurvey['title'],
            'user_id' => Auth::id()
        ]);

        foreach ($inputSurvey['questions'] as $question) {
            $answer = AnswerFactory::createFromArray($question['answer']['type'], $question['answer']);
            Question::create([
                'text' => $question['text'],
                'survey_id' => $survey->id,
                'answer_id' => $answer->id,
                'answer_type' => get_class($answer)
            ]);
        }

        return new Response(['surveyId' => $survey->id], 201);
    }

    public function surveys()
    {
        $surveys = Survey::where('user_id', Auth::id())->withCount('respondents')->get();
        return new Response($surveys, 200);
    }

    public function surveyAnswers(string $surveyId)
    {
        /** @var Survey $survey */
        $survey = Survey::where('id', $surveyId)->get()->first();
        if ($survey === null) {
            return new Response(null, 404);
        }

        $results = (new SurveyResultsService())->getResultsForSurvey($survey);

        return new Response($results, 200);
    }


    public function export(string $surveyId)
    {
        $survey = Survey::where('id', $surveyId)->get()->first();
        if ($survey === null) {
            return new Response(null, 404);
        }

        return (new SurveyExport)->forSurvey($surveyId)->download('survey.xlsx');
    }
}
