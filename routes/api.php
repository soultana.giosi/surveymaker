<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'admin'
], function() {
    Route::post('surveys', 'AdminSurveyController@create');
    Route::get('surveys', 'AdminSurveyController@surveys');
    Route::get('surveys/{surveyId}/answers', 'AdminSurveyController@surveyAnswers');
});

Route::get('surveys/{surveyId}/export', 'AdminSurveyController@export');

Route::get('surveys/{surveyId}', 'SurveyController@single');
Route::post('surveys/{surveyId}', 'SurveyController@answer');
