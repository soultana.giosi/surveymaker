
### docker-compose commands
```
docker-compose up -d ( start containers and detach )
docker-compose ps ( see containers )
docker-compose down ( kill containers )
docker-compose exec app bash ( start bash in app container )
```

### Setup

- `composer install` (install php dependencies)
- `npm install` (install js dependencies)
- create `.env` file 
- `php artisan key:generate`
- `php artisan passport:keys`
- `php artisan passport:client --personal`
- `php artisan migrate` (create db tables)
- `npm run development` OR `npm run watch` (build js) 
