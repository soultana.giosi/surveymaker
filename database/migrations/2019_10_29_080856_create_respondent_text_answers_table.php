<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespondentTextAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_text_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('text_answer_id')->nullable();
            $table->text('answer');
            $table->timestamps();

            $table->foreign('text_answer_id')
                ->references('id')->on('text_answers')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respondent_text_answers', function (Blueprint $table) {
            $table->dropForeign(['text_answer_id']);
            $table->dropIfExists();
        });
    }
}
