<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultipleChoiceAnswerOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiple_choice_answer_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('text');
            $table->unsignedBigInteger('multiple_choice_answer_id')->nullable();
            $table->timestamps();

            $table->foreign('multiple_choice_answer_id')
                ->references('id')->on('multiple_choice_answers')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('multiple_choice_answer_options', function (Blueprint $table) {
            $table->dropForeign(['multiple_choice_answer_id']);
            $table->dropIfExists();
        });
    }
}
